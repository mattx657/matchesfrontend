import axios from "axios";

import { useState, useEffect } from "react";

export const useUsers = () => {
  const [users, setUsers] = useState();
  const [error, setError] = useState();
  function logout() {
    localStorage.clear();
    window.location.href = `join-us`;
  }
  useEffect(() => {
    const controller = new AbortController();
    const { signal } = controller;
    axios
      .get("/users", { signal })
      .then((userFromApi) => setUsers(userFromApi.data))
      .catch((apiError) => setError(apiError));

    return () => controller.abort();
  }, []);
  return [error, users, logout];
};
