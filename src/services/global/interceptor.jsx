import axios from "axios";
import { isProductionEnv } from "../util";
axios.interceptors.request.use(
  (config) => {
    const isProd = isProductionEnv();
    const token = localStorage.getItem("token");

    if (token) {
      config.headers.Authorization = `${token}`;
    }
    if (isProd) {
      config.url = `https://matches.mokosz.net${config.url}`;
    } else {
      config.url = `http://localhost:3000${config.url}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

//   axios.interceptors.response.use(
//     response => {
//       return response
//     },
//     function (error) {
//       const originalRequest = error.config

//       if (
//         error.response.status === 401 &&
//         originalRequest.url === 'http://127.0.0.1:3000/v1/auth/token'
//       ) {
//         router.push('/login')
//         return Promise.reject(error)
//       }

//       if (error.response.status === 401 && !originalRequest._retry) {
//         originalRequest._retry = true
//         const refreshToken = localStorageService.getRefreshToken()
//         return axios
//           .post('/auth/token', {
//             refresh_token: refreshToken
//           })
//           .then(res => {
//             if (res.status === 201) {
//               localStorageService.setToken(res.data)
//               axios.defaults.headers.common['Authorization'] =
//                 'Bearer ' + localStorageService.getAccessToken()
//               return axios(originalRequest)
//             }
//           })
//       }
//       return Promise.reject(error)
//     }
//   )
