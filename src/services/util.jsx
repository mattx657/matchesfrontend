const isProd=null;
export const isProductionEnv=()=>{
    if(isProd!==null){
        return isProd;
    }
    return import.meta.env.MODE !=='development'
}