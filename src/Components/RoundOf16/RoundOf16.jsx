import "./RoundOf16.css";
import axios from "axios";
import { Component } from "react";

class RoundOf16 extends Component {
  constructor() {
    super();
    this.state = { players: [] };
  }

  async componentDidMount() {
    this.players = (await axios.get("/ranking")).data;
    this.setState({ players: this.players });
  }

  render() {
    const { players } = this.state;

    players.sort((a, b) => b.score - a.score);

    let rank = 1;
    let ranks = [];
    for (let i = 0; i < players.length; i++) {
      if (i > 0 && players[i].score < players[i - 1].score) {
        rank = i + 1;
      }
      ranks[players[i].id] = rank;
    }

    return (
      <div className="round-of-16-container">
        <div className="round-of-16">
          <table className="table-background">
            <thead>
              <tr>
                <th className="table-background">No.</th>
                <th className="table-background">Name</th>
                <th className="table-background">Points</th>
              </tr>
            </thead>
            <tbody>
              {players.map((player, index) => {
                const rank = ranks[player.id];
                let className = "column-container";
                let medalEmoji = "";

                if (rank === 1) {
                  className += " gold";
                  medalEmoji = "🥇";
                } else if (rank === 2) {
                  className += " silver";
                  medalEmoji = "🥈";
                } else if (rank === 3) {
                  className += " bronze";
                  medalEmoji = "🥉";
                }

                return (
                  <tr key={player.id} className="player">
                    <td className={className}>
                      {rank} {medalEmoji}
                    </td>
                    <td className={className}>
                      {player.firstname} {player.lastname}
                    </td>
                    <td className={className}>{player.score}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default RoundOf16;
