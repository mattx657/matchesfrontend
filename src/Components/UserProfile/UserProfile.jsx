import { Component } from "react";
import "./UserProfile.css";
import { useUsers } from "../../services/global/user.service";
class UserProfile extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}
  render() {
    // eslint-disable-next-line react/prop-types
    const user = this.props.user;

    // eslint-disable-next-line react/prop-types
    const logout = this.props.logout;
    if (!user) {
      return <div>Loading...</div>;
    }
    return (
      <div className="container-login-panel ">
        <h1 className="header">
          Hi {user.firstname} {user.lastname}
        </h1>
        <p className="user-points">Your points:{user.score}</p>
        <button className="logout-button" onClick={logout}>
          Logout
        </button>
      </div>
    );
  }
}

// eslint-disable-next-line react-refresh/only-export-components
export default injectUser(UserProfile);

function injectUser(Component) {
  const InjectedUser = function (props) {
    const user = useUsers(props);
    return <Component {...props} user={user[1]} logout={user[2]} />;
  };
  return InjectedUser;
}
