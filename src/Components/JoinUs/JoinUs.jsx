import { useState } from "react";
import axios from "axios";
import { FaUser, FaLock, FaEnvelope } from "react-icons/fa";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "./JoinUs.css";

const JoinUs = () => {
  const [action, setAction] = useState("");
  const [formData, setFormData] = useState({
    email: "",
    password: "",
    firstName: "",
    lastName: "",
  });
  const [errorMessage, setErrorMessage] = useState("");
  const [successMessage, setSuccessMessage] = useState("");

  const registerLink = () => {
    setAction("active");
    setErrorMessage("");
  };

  const loginLink = () => {
    setAction("");
    setErrorMessage("");
  };

  const handleInputChange = (e) => {
    setFormData({
      ...formData,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post("/users/login", {
        email: formData.email,
        password: formData.password,
      });
      toast.success("Login successful!");
      localStorage.setItem("token", response.data.token);
      window.location.href = "/matches";
      setErrorMessage("");
    } catch (error) {
      console.error("Error logging in:", error);
      toast.error("Invalid email or password. Please try again.");
    }
  };

  const handleCreate = async (event) => {
    event.preventDefault();
    try {
      await axios.post("/users/create", {
        email: formData.email,
        password: formData.password,
        firstName: formData.firstName,
        lastName: formData.lastName,
      });
      toast.success("Registration successful!, Please login", {
        location: "bottom-center",
      });
      //localStorage.setItem("token", response.data.token);
      setAction("");
      setErrorMessage("");
    } catch (error) {
      console.error("Error creating account:", error);
      toast.error("Error creating account. Please try again.");
    }
  };

  return (
    <div className={`wrapper ${action}`}>
      <div className="form-box login">
        <form onSubmit={handleSubmit}>
          <h1 className="phone">Login</h1>
          <div className="input-box">
            <input
              type="text"
              name="email"
              value={formData.email}
              onChange={handleInputChange}
              placeholder="Email"
              required
            />
            <FaUser className="icon" />
          </div>
          <div className="input-box">
            <input
              type="password"
              name="password"
              value={formData.password}
              onChange={handleInputChange}
              placeholder="Password"
              required
            />
            <FaLock className="icon" />
          </div>
          {errorMessage && <p className="error-message">{errorMessage}</p>}
          {successMessage && (
            <p className="success-message">{successMessage}</p>
          )}
          <div className="remember-forgot">
            <label>
              {/* <input type="checkbox" />
              Remember me */}
            </label>
            <a href="#">Forgot password?</a>
          </div>
          <button type="submit">Login</button>
          <div className="register-link">
            <p className="pelement">
              Dont have an account?
              <a href="#" onClick={registerLink}>
                Register
              </a>
            </p>
          </div>
        </form>
      </div>
      <div className="form-box register">
        <form onSubmit={handleCreate}>
          <h1 className="phone">Registration</h1>

          <div className="input-box">
            <input
              type="email"
              name="email"
              value={formData.email}
              onChange={handleInputChange}
              placeholder="Email"
              required
            />
            <FaEnvelope className="icon" />
          </div>
          <div className="input-box">
            <input
              type="password"
              name="password"
              value={formData.password}
              onChange={handleInputChange}
              placeholder="Password"
              required
            />
            <FaLock className="icon" />
          </div>
          <div className="input-box">
            <input
              type="text"
              name="firstName"
              value={formData.firstName}
              onChange={handleInputChange}
              placeholder="Imie"
              required
            />
            <FaUser className="icon" />
          </div>
          <div className="input-box">
            <input
              type="text"
              name="lastName"
              value={formData.lastName}
              onChange={handleInputChange}
              placeholder="Nazwisko"
              required
            />
            <FaUser className="icon" />
          </div>
          {errorMessage && <p className="error-message">{errorMessage}</p>}
          {successMessage && (
            <p className="success-message">{successMessage}</p>
          )}
          <div className="remember-forgot">
            <label>
              {/* <input type="checkbox" />I agree to the terms & conditions{" "} */}
            </label>
          </div>
          <button type="submit">Register</button>
          <div className="register-link">
            <p className="pelement">
              Already have an account?
              <a href="#" onClick={loginLink}>
                Login
              </a>
            </p>
          </div>
        </form>
      </div>
      <ToastContainer position="top-center" />{" "}
    </div>
  );
};

export default JoinUs;
