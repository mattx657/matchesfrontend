import "./Groups.css";

const Groups = () => {
  return (
    <div className="container">
      <div className="group">
        <h2>Grupa A</h2>
        <ul>
          <li>
            Germany <span className="fi fi-de"></span>
          </li>
          <li>
            Scotland <span className="fi fi-gb-sct"></span>
          </li>
          <li>
            Hungary <span className="fi fi-hu"></span>
          </li>
          <li>
            Switzerland <span className="fi fi-ch"></span>
          </li>
        </ul>
      </div>
      <div className="group">
        <h2>Grupa B</h2>
        <ul>
          <li>
            Spain <span className="fi fi-es"></span>
          </li>
          <li>
            Croatia <span className="fi fi-hr"></span>
          </li>
          <li>
            Italy <span className="fi fi-it"></span>
          </li>
          <li>
            Albania <span className="fi fi-al"></span>
          </li>
        </ul>
      </div>
      <div className="group">
        <h2>Grupa C</h2>
        <ul>
          <li>
            Slovenia <span className="fi fi-si"></span>
          </li>
          <li>
            Denmark <span className="fi fi-dk"></span>
          </li>
          <li>
            Serbia <span className="fi fi-rs"></span>
          </li>
          <li>
            England <span className="fi fi-gb-eng"></span>
          </li>
        </ul>
      </div>
      <div className="group">
        <h2>Grupa D</h2>
        <ul>
          <li>
            Netherlands <span className="fi fi-nl"></span>
          </li>
          <li>
            France <span className="fi fi-fr"></span>
          </li>
          <li>
            Poland <span className="fi fi-pl"></span>
          </li>
          <li>
            Austria <span className="fi fi-at"></span>
          </li>
        </ul>
      </div>
      <div className="group">
        <h2>Grupa E</h2>
        <ul>
          <li>
            Ukraine <span className="fi fi-ua"></span>
          </li>
          <li>
            Slovakia <span className="fi fi-sk"></span>
          </li>
          <li>
            Belgium <span className="fi fi-be"></span>
          </li>
          <li>
            Romania <span className="fi fi-ro"></span>
          </li>
        </ul>
      </div>
      <div className="group">
        <h2>Grupa F</h2>
        <ul>
          <li>
            Portugal <span className="fi fi-pt"></span>
          </li>
          <li>
            Czechia <span className="fi fi-cz"></span>
          </li>
          <li>
            Georgia <span className="fi fi-ge"></span>
          </li>
          <li>
            Turkiye <span className="fi fi-tr"></span>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Groups;
