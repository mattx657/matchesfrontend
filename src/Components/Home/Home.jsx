import { useState } from "react";
import { Link } from "react-router-dom";
import { useEffect } from "react";
import "./Home.css";

const Home = () => {
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    const joinUsButton = document.querySelector(".join-us-button");
    const windowHeight = window.innerHeight;

    function checkButtonVisibility() {
      const buttonPosition = joinUsButton.getBoundingClientRect().top;

      if (buttonPosition - windowHeight < 0) {
        joinUsButton.classList.add("appear");
        window.removeEventListener("scroll", checkButtonVisibility);
      }
    }

    window.addEventListener("scroll", checkButtonVisibility);
    checkButtonVisibility();

    return () => {
      window.removeEventListener("scroll", checkButtonVisibility);
    };
  }, []);

  const toggleModal = () => {
    setModalOpen(!modalOpen);
  };

  return (
    <div className="home-container">
      <h1 className="welcome-text">Welcome</h1>
      <h2 className="kickoff-text">to KickOff game</h2>

      <button className="join-us-button" onClick={toggleModal}>
        Lets begin!
      </button>

      {modalOpen && (
        <div className="modal-overlay">
          <div className="modal">
            <button className="close-modal" onClick={toggleModal}>
              Close
            </button>
            <p>
              <h1> Witamy w KickOff!</h1>
              Gra KickOff polega na obstawianiu meczów piłkarskich turnieju
              <strong> Euro 2024</strong>. Jeśli chcesz do nas dołączyć i
              przeżyć trochę emocji, możesz założyć konto i rozpocząć
              obstawianie. Głównym warunkiem rozpoczęcia gry jest wejście do
              niej za stawkę
              <strong> 50zł</strong> .
              <p>
                Każdy mecz można obstawić maksymalnie na godzinę przed jego
                rozpoczęciem. Za każdy prawidłowo obstawiony mecz gracz
                otrzymuje jeden punkt. Możesz sprawdzić rankingi w panelu
                użytkownika. Po turnieju zwycięzca otrzymuje całą pulę pieniędzy
                więc jest o co walczyć. Powodzenia!🏆⚽
              </p>
            </p>
            <Link to="/join-us" className="join-us-modal-button">
              Join Us
            </Link>
          </div>
        </div>
      )}
    </div>
  );
};

export default Home;
