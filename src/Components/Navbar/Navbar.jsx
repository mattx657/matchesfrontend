// import "./Navbar.css";
// // import { componentDidMount } from "react";
// import { FaRankingStar } from "react-icons/fa6";
// import { FaPeopleGroup } from "react-icons/fa6";
// import { IoIosFootball } from "react-icons/io";
// import { IoMdLogIn } from "react-icons/io";
// import { FaTrophy } from "react-icons/fa6";

// // function Navbar() {
// //   const list = document.querySelectorAll(".list");
// //   componentDidMount(() => {
// //     alert("test");
// //   });
// //   list.forEach((item) =>
// //     item.addEventListener("click", () => {
// //       list.forEach((item) => item.classList.remove("active"));
// //       this.classList.add("active");
// //     })
// //   );
// //   return (
// //     <div className="navigation">
// //       <ul>
// //         <li className="list active">
// //           <a href="#">
// //             <span className="icon">
// //               <FaRankingStar />
// //             </span>
// //             <span className="text">Ranking</span>
// //             <span className="circle"></span>
// //           </a>
// //         </li>
// //         <li className="list">
// //           <a href="#">
// //             <span className="icon">
// //               <FaPeopleGroup />
// //             </span>
// //             <span className="text">Groups</span>
// //             <span className="circle"></span>
// //           </a>
// //         </li>
// //         <li className="list">
// //           <a href="#">
// //             <span className="icon">
// //               <IoIosFootball />
// //             </span>
// //             <span className="text">Matches</span>
// //             <span className="circle"></span>
// //           </a>
// //         </li>

// //         <li className="list">
// //           <a href="#">
// //             <span className="icon">
// //               <FaTrophy />
// //             </span>
// //             <span className="text">Round of 16</span>
// //             <span className="circle"></span>
// //           </a>
// //         </li>
// //         <li className="list">
// //           <a href="#">
// //             <span className="icon">
// //               <IoMdLogIn />
// //             </span>
// //             <span className="text">Join us</span>
// //             <span className="circle"></span>
// //           </a>
// //         </li>
// //         <div className="indicator"></div>
// //       </ul>
// //     </div>
// //   );
// // }

// // export default Navbar;
// import { Component } from "react";
// class Navbar extends Component {
//   constructor() {
//     super();
//     const list = document.querySelectorAll(".list");
//     console.log(list);
//   }
//   componentDidMount() {
//     const list = document.querySelectorAll(".list");
//     console.log(list);
//     // const list = document.querySelectorAll(".list");
//     // list.forEach((item) =>
//     //   item.addEventListener("click", (ele) => {
//     //     list.forEach((item) => item.classList.remove("active"));
//     //     ele.classList.add("active");
//     //   })
//     // );
//   }
//   makeActive(ele) {
//     const list = document.querySelectorAll(".list");
//     list.forEach((item) => item.classList.remove("active"));
//     ele.currentTarget.classList.add("active");
//   }
//   render() {
//     return (
//       <div className="navigation">
//         <ul>
//           <li className="list active" onClick={this.makeActive.bind(this)}>
//             <a href="#">
//               <span className="icon">
//                 <FaRankingStar />
//               </span>
//               <span className="text">Ranking</span>
//               <span className="circle"></span>
//             </a>
//           </li>
//           <li className="list" onClick={this.makeActive.bind(this)}>
//             <a href="#">
//               <span className="icon">
//                 <FaPeopleGroup />
//               </span>
//               <span className="text">Groups</span>
//               <span className="circle"></span>
//             </a>
//           </li>
//           <li className="list" onClick={this.makeActive.bind(this)}>
//             <a href="#">
//               <span className="icon">
//                 <IoIosFootball />
//               </span>
//               <span className="text">Matches</span>
//               <span className="circle"></span>
//             </a>
//           </li>

//           <li className="list" onClick={this.makeActive.bind(this)}>
//             <a href="#">
//               <span className="icon">
//                 <FaTrophy />
//               </span>
//               <span className="text">Round of 16</span>
//               <span className="circle"></span>
//             </a>
//           </li>
//           <li className="list" onClick={this.makeActive.bind(this)}>
//             <a href="#">
//               <span className="icon">
//                 <IoMdLogIn />
//               </span>
//               <span className="text">Join us</span>
//               <span className="circle"></span>
//             </a>
//           </li>
//           <div className="indicator"></div>
//         </ul>
//       </div>
//     );
//   }
// }
// export default Navbar;
import { Component } from "react";
import { Link } from "react-router-dom";
import { FaPeopleGroup, FaTrophy } from "react-icons/fa6";
import { IoIosFootball, IoMdLogIn, IoMdHome } from "react-icons/io";
import { FaUser } from "react-icons/fa";
import "./Navbar.css";
import { useUsers } from "../../services/global/user.service";
class Navbar extends Component {
  constructor(props) {
    super(props);
  }
  makeActive(ele) {
    const list = document.querySelectorAll(".list");
    list.forEach((item) => item.classList.remove("active"));
    if (ele.currentTarget) {
      ele.currentTarget.classList.add("active");
    } else {
      ele.classList.add("active");
    }
  }
  componentDidMount() {
    const href = window.location.href;

    document.querySelectorAll(".list").forEach((ele) => {
      if (ele.childNodes[0].href === href) {
        this.makeActive(ele);
      }
    });
  }
  render() {
    // eslint-disable-next-line react/prop-types
    const user = this.props.user;
    let userElement;
    if (user) {
      userElement = (
        <li className="list" onClick={this.makeActive.bind(this)}>
          <Link to="/user-profile">
            <span className="icon ">
              <FaUser />
            </span>
            <span className="text">User</span>
            <span className="circle"></span>
          </Link>
        </li>
      );
    } else {
      userElement = (
        <li className="list" onClick={this.makeActive.bind(this)}>
          <Link to="/join-us">
            <span className="icon">
              <IoMdLogIn />
            </span>
            <span className="text">Join us</span>
            <span className="circle"></span>
          </Link>
        </li>
      );
    }
    return (
      <div className="navigation">
        <ul>
          <li className="list" onClick={this.makeActive.bind(this)}>
            <Link to="/home">
              <span className="icon">
                <IoMdHome />{" "}
              </span>
              <span className="text">Home</span>
              <span className="circle"></span>
            </Link>
          </li>
          <li className="list" onClick={this.makeActive.bind(this)}>
            <Link to="/groups">
              <span className="icon">
                <FaPeopleGroup />
              </span>
              <span className="text">Groups</span>
              <span className="circle"></span>
            </Link>
          </li>
          <li className="list" onClick={this.makeActive.bind(this)}>
            <Link to="/matches">
              <span className="icon">
                <IoIosFootball />
              </span>
              <span className="text">Matches</span>
              <span className="circle"></span>
            </Link>
          </li>
          <li className="list" onClick={this.makeActive.bind(this)}>
            <Link to="/round-of-16">
              <span className="icon">
                <FaTrophy />
              </span>
              <span className="text">Ranking</span>
              <span className="circle"></span>
            </Link>
          </li>
          {userElement}
          <div className="indicator"></div>
        </ul>
      </div>
    );
  }
}

// eslint-disable-next-line react-refresh/only-export-components
export default injectUser(Navbar);

function injectUser(Component) {
  const InjectedUser = function (props) {
    const user = useUsers(props);
    return <Component {...props} user={user[1]} logout={user[2]} />;
  };
  return InjectedUser;
}
