import { Component } from "react";
import axios from "axios";
import { IoMdFootball } from "react-icons/io";
import "./GameContainer.css";

class GameContainer extends Component {
  constructor(props) {
    super(props);
    // eslint-disable-next-line react/prop-types
    let game_date = this.props.gameInfo.game_date.replace("Z", "");
    game_date = new Date(game_date);
    let isGameDisabled = false;
    if (new Date() > game_date.setHours(game_date.getHours() - 1)) {
      isGameDisabled = true;
    }
    this.state = {
      // eslint-disable-next-line react/prop-types
      gameInfo: this.props.gameInfo,
      // eslint-disable-next-line react/prop-types
      refreshData: this.props.refreshData,
      isGameDisabled: isGameDisabled,
      gamePrediction: null,
    };
  }

  async componentDidMount() {}
  async makeApiCall() {
    const { gameInfo } = this.state;
    const gameId = gameInfo.id;
    const countryId = gameInfo.countryBet1
      ? gameInfo.country_1
      : gameInfo.country_2;
    try {
      await axios.post(
        "/match",
        {
          gameId: gameId,
          betCountryId: countryId,
          betExtraTime: this.state.gameInfo.countryExtraTime
            ? this.state.gameInfo.extra_time
            : 0,
          betPenaltyResult: this.state.gameInfo.countryPenalty
            ? this.state.gameInfo.win_by_penalty
            : 0,
          bet_country_phase_id: gameInfo?.playerBet?.phasegameid,
        },
        {
          headers: {
            Authorization: `${localStorage.getItem("token")}`,
          },
        }
      );
    } catch (response) {
      if (response.response.status === 401) {
        localStorage.clear();
        window.location.href = `join-us`;
      }
      if (response.response.status === 405) {
        this.setState({ isGameDisabled: true });
        return;
      }
    }
  }
  async chooseCountry(countryId, gameId) {
    const { gameInfo } = this.state;

    gameInfo.countryBet1 = false;
    gameInfo.countryBet2 = false;
    gameInfo.countryExtraTime = false;
    gameInfo.countryPenalty = false;

    if (countryId === gameInfo.country_1) {
      gameInfo.countryBet1 = true;
    } else if (countryId === gameInfo.country_2) {
      gameInfo.countryBet2 = true;
    } else if (countryId === gameInfo.extra_time) {
      gameInfo.countryExtraTime = true;
    } else if (countryId === gameInfo.win_by_penalty) {
      gameInfo.countryPenalty = true;
    }
    if (gameInfo.playerBet) {
      gameInfo.playerBet.phasegameid = 0;
    }
    this.setState({
      gameInfo: { ...gameInfo },
    });
    await this.makeApiCall();
  }
  async chooseGamePhase(phasegameid) {
    const { gameInfo } = this.state;

    if (gameInfo.playerBet) {
      gameInfo.playerBet.phasegameid = phasegameid;
    }

    this.setState({
      gameInfo: { ...gameInfo },
    });
    await this.makeApiCall();
  }
  getParsedDate(strDate) {
    let strSplitDate = String(strDate).split(" ")[0];
    strSplitDate = strSplitDate.replace("Z", "");

    let date = new Date(strSplitDate);
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    const hh = date.getHours();
    let min = date.getMinutes();
    if (min === 0) {
      min = "00";
    }
    const yyyy = date.getFullYear();
    if (dd < 10) {
      dd = "0" + dd;
    }
    if (mm < 10) {
      mm = "0" + mm;
    }
    date = `${dd}-${mm}-${yyyy} ${hh}:${min}`;
    return date.toString();
  }

  render() {
    const { gameInfo, isGameDisabled } = this.state;

    const additionalOptions = (
      <div className="flex">
        <button
          disabled={isGameDisabled}
          onClick={() => this.chooseGamePhase(1)}
          className={`draw-button ${
            gameInfo.playerBet?.phasegameid === 1
              ? "country-selected selected"
              : ""
          } ${
            gameInfo.won_country_id === 0
              ? "winner-button"
              : gameInfo?.countryExtraTime &&
                gameInfo.won_country_id !== 0 &&
                isGameDisabled &&
                gameInfo.country_score_1 !== null &&
                gameInfo.country_score_2 !== null
              ? "loser-button"
              : ""
          } ${isGameDisabled ? "game-disabled" : ""}`}
        >
          <IoMdFootball className="icondraw" />
          <p>
            Regulaminowy czas<span className="highlight"> (3pkt)</span>{" "}
          </p>
        </button>
        <button
          disabled={isGameDisabled}
          onClick={() => this.chooseGamePhase(2)}
          className={`draw-button ${
            gameInfo.playerBet?.phasegameid === 2
              ? "country-selected selected"
              : ""
          } ${
            gameInfo.won_country_id === 0
              ? "winner-button"
              : gameInfo?.countryExtraTime &&
                gameInfo.won_country_id !== 0 &&
                isGameDisabled &&
                gameInfo.country_score_1 !== null &&
                gameInfo.country_score_2 !== null
              ? "loser-button"
              : ""
          } ${isGameDisabled ? "game-disabled" : ""}`}
        >
          <IoMdFootball className="icondraw" />
          <p>
            Dogrywka<span className="highlight"> (3pkt)</span>{" "}
          </p>
        </button>

        <button
          disabled={isGameDisabled}
          onClick={() => this.chooseGamePhase(3)}
          className={`draw-button ${
            gameInfo.playerBet?.phasegameid === 3
              ? "country-selected selected"
              : ""
          } ${
            gameInfo.won_country_id === 0
              ? "winner-button"
              : gameInfo?.countryPenaltyTime &&
                gameInfo.won_country_id !== 0 &&
                isGameDisabled &&
                gameInfo.country_score_1 !== null &&
                gameInfo.country_score_2 !== null
              ? "loser-button"
              : ""
          } ${isGameDisabled ? "game-disabled" : ""}`}
        >
          <IoMdFootball className="icondraw" />
          <p>
            Rzuty karne<span className="highlight"> (4pkt)</span>
          </p>
        </button>
      </div>
    );
    let additionalOptionsCountry;
    if (gameInfo?.countryBet1 || gameInfo?.countryBet2) {
      additionalOptionsCountry = additionalOptions;
    }
    return (
      <div className="game-container" id={gameInfo.id}>
        <p className="game-date">
          Zwycięzca (2pkt) Dodatkowy bet (3pkt) Łącznie do zdobycia (5pkt)
        </p>
        <p className={`game-date ${isGameDisabled ? "game-disabled" : ""}`}>
          {this.getParsedDate(gameInfo.game_date)}
        </p>
        <div className="button-container">
          <button
            disabled={isGameDisabled}
            onClick={() => this.chooseCountry(gameInfo.country_1, gameInfo.id)}
            className={`team-button ${
              gameInfo?.countryBet1 ? "country-selected selected" : ""
            } ${
              gameInfo.won_country_id === gameInfo.country_1
                ? "winner-button"
                : gameInfo?.countryBet1 &&
                  gameInfo.won_country_id !== gameInfo.country_1 &&
                  isGameDisabled &&
                  gameInfo.country_score_1 !== null &&
                  gameInfo.country_score_2 !== null
                ? "loser-button"
                : ""
            } ${isGameDisabled ? "game-disabled" : ""}`}
          >
            <img
              src={gameInfo.countryInfo1.countryimg}
              alt={`${gameInfo.countryInfo1.title} flag`}
              width={64}
              className={`country__image ${
                gameInfo?.countryBet1 === true ? "center-img" : ""
              }`}
            />
            <p>
              {gameInfo.countryInfo1.title}
              <span className="highlight"> (2pkt)</span>
            </p>
          </button>
          <button
            disabled={isGameDisabled}
            onClick={() => this.chooseCountry(gameInfo.country_2, gameInfo.id)}
            className={`team-button ${
              gameInfo?.countryBet2 ? "country-selected selected" : ""
            } ${
              gameInfo.won_country_id === gameInfo.country_2
                ? "winner-button"
                : gameInfo?.countryBet2 &&
                  gameInfo.won_country_id !== gameInfo.country_2 &&
                  isGameDisabled &&
                  gameInfo.country_score_1 !== null &&
                  gameInfo.country_score_2 !== null
                ? "loser-button"
                : ""
            } ${isGameDisabled ? "game-disabled" : ""}`}
          >
            <img
              src={gameInfo.countryInfo2.countryimg}
              alt={`${gameInfo.countryInfo2.title} flag`}
              width={64}
              className={`country__image ${
                gameInfo?.countryBet2 === true ? "center-img" : ""
              }`}
            />
            <p>
              {gameInfo.countryInfo2.title}
              <span className="highlight"> (2pkt)</span>
            </p>
          </button>
        </div>
        {additionalOptionsCountry}
        {isGameDisabled && (
          <div className="disabled-text-container">
            {gameInfo.country_score_1 !== null &&
            gameInfo.country_score_2 !== null ? (
              <p className="country-score">
                {gameInfo.country_score_1}:{gameInfo.country_score_2}
              </p>
            ) : (
              <p className="disabled-text">
                Nie możesz już obstawić tego meczu
              </p>
            )}
          </div>
        )}

        {isGameDisabled && (
          <div className="prediction-info">
            <p className="prediction-info2">Jak obstawiali gracze:</p>
            <ul className="prediction-list">
              <li>
                {gameInfo.predCountry_1 != null ? gameInfo.predCountry_1 : 0}
              </li>
              <li>
                {gameInfo.predCountry_draw != null
                  ? gameInfo.predCountry_draw
                  : 0}
              </li>
              <li>
                {gameInfo.predCountry_2 != null ? gameInfo.predCountry_2 : 0}
              </li>
            </ul>
          </div>
        )}
      </div>
    );
  }
}
export default GameContainer;
