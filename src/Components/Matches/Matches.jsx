// import "./Matches.css";
// import { Component } from "react";

// import axios from "axios";
// import GameContainer from "./game-container/GameContainer";

// class Matches extends Component {
//   //request to get games and country

//   constructor() {
//     super();
//     this.state = { games: [] };
//   }
//   async componentDidMount() {
//     this.gamesResponse = (await axios.get("/match/game")).data;
//     const todayMatch = this.gamesResponse.find(
//       (x) =>
//         new Date(x.game_date.replace("Z", "")).getDate() ===
//         new Date().getDate()
//     );
//     this.countries = (await axios.get("/match/getAllCountry")).data;
//     await this.refreshData();
//     setTimeout(() => {
//       this.scrollToFirstActiveButton(todayMatch.id);
//     }, 500);
//   }

//   scrollToFirstActiveButton(id) {
//     const firstActiveButton = document.getElementById(`${id}`);
//     if (firstActiveButton) {
//       firstActiveButton.scrollIntoView({ behavior: "smooth", block: "start" });
//     }
//   }

//   async refreshData() {
//     let playerBets = [];
//     try {
//       playerBets = (await axios.get("/match/gamePlayer"))?.data;
//     } catch (e) {}

//     for (let index = 0; index < this.gamesResponse.length; index++) {
//       let game = this.gamesResponse[index];
//       const playerBet = playerBets?.find((x) => x.gameid === game.id);
//       if (playerBet) {
//         game.playerBet = playerBet;
//         game.countryBet1 = false;
//         game.countryBet2 = false;
//         game.draw = false;

//         if (game.country_1 === playerBet.betcountryid) {
//           game.countryBet1 = true;
//         } else if (game.country_2 === playerBet.betcountryid) {
//           game.countryBet2 = true;
//         } else if (playerBet.betcountryid == 0) {
//           game.draw = true;
//         }
//       }
//       game.countryInfo1 = this.countries.find((x) => x.id === game.country_1);
//       game.countryInfo2 = this.countries.find((x) => x.id === game.country_2);
//       setTimeout(() => {}, 100);
//     }
//     this.setState({ games: this.gamesResponse });
//   }

//   render() {
//     return (
//       <div>
//         <div className="matches__background">
//           <h1>Obstawianie meczów</h1>
//           {this.state.games.length == 0 ? (
//             <div className="loading-text">Loading matches...</div>
//           ) : (
//             this.state.games.map((game) => (
//               <GameContainer
//                 key={game.id}
//                 gameInfo={game}
//                 refreshData={this.refreshData.bind(this)}
//               />
//             ))
//           )}
//         </div>
//       </div>
//     );
//   }
// }

// export default Matches;
import "./Matches.css";
import { Component } from "react";

import axios from "axios";
import GameContainer from "./game-container/GameContainer";

class Matches extends Component {
  //request to get games and country

  constructor() {
    super();
    this.state = { games: [] };
  }
  async componentDidMount() {
    this.gamesResponse = (await axios.get("/match/game")).data;
    const todayMatch = this.gamesResponse.find(
      (x) =>
        new Date(x.game_date.replace("Z", "")).getDate() ===
        new Date().getDate()
    );
    this.countries = (await axios.get("/match/getAllCountry")).data;
    await this.refreshData();
    if (todayMatch) {
      setTimeout(() => {
        this.scrollToFirstActiveButton(todayMatch.id);
      }, 500);
    }
  }

  scrollToFirstActiveButton(id) {
    const firstActiveButton = document.getElementById(`${id}`);
    if (firstActiveButton) {
      firstActiveButton.scrollIntoView({ behavior: "smooth", block: "start" });
    }
  }

  async refreshData() {
    let playerBets = [];

    playerBets = (await axios.get("/match/gamePlayer"))?.data;

    for (let index = 0; index < this.gamesResponse.length; index++) {
      let game = this.gamesResponse[index];
      const playerBet = playerBets?.find((x) => x.gameid === game.id);
      if (playerBet) {
        game.playerBet = playerBet;
        game.countryBet1 = false;
        game.countryBet2 = false;
        game.draw = false;

        if (game.country_1 === playerBet.betcountryid) {
          game.countryBet1 = true;
        } else if (game.country_2 === playerBet.betcountryid) {
          game.countryBet2 = true;
        } else if (playerBet.betcountryid == 0) {
          game.draw = true;
        }
      }
      game.countryInfo1 = this.countries.find((x) => x.id === game.country_1);
      game.countryInfo2 = this.countries.find((x) => x.id === game.country_2);
      setTimeout(() => {}, 100);
    }
    this.setState({ games: this.gamesResponse });
  }

  render() {
    return (
      <div>
        <div className="matches__background">
          <h1>Obstawianie meczów</h1>
          {this.state.games.length == 0 ? (
            <div className="loading-text">Loading matches...</div>
          ) : (
            this.state.games.map((game) => (
              <GameContainer
                key={game.id}
                gameInfo={game}
                refreshData={this.refreshData.bind(this)}
              />
            ))
          )}
        </div>
      </div>
    );
  }
}

export default Matches;
