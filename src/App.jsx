import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import "./services/global/interceptor";
import Navbar from "./Components/Navbar/Navbar";
import Home from "./Components/Home/Home";
import Groups from "./Components/Groups/Groups";
import Matches from "./Components/Matches/Matches";
import RoundOf16 from "./Components/RoundOf16/RoundOf16";
import JoinUs from "./Components/JoinUs/JoinUs";
//import { getUserToken, setUser } from "./services/global/user.service";
import axios from "axios";
import UserProfile from "./Components/UserProfile/UserProfile";

const App = () => {
  // if (getUserToken()) {
  //   axios.get("/users").then((response) => {
  //     const user = response.data;
  //     setUser(user);
  //   });
  // }
  return (
    <Router>
      <Navbar />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/home" element={<Home />} />
        <Route path="/groups" element={<Groups />} />
        <Route path="/matches" element={<Matches />} />
        <Route path="/round-of-16" element={<RoundOf16 />} />
        <Route path="/join-us" element={<JoinUs />} />
        <Route path="/user-profile" element={<UserProfile />} />
      </Routes>
    </Router>
  );
};

export default App;
